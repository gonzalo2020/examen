export const dummyData = [
    {
        title: 'Cervezas',
        url: 'https://www.mundocerveza.com/wp-content/uploads/2016/07/image-3.jpeg',
        descripcion: "Todo tipo de cervezas",
        id:1
    },{
        title: 'Rom',
        url: 'https://revistapym.com.co/wp-content/uploads/2016/08/licores.jpg',
        descripcion:"Todo tipo de rom",
        id:2
    },{
        title: 'Whisky',
        url: 'https://s1.eestatic.com/2019/04/04/ciencia/nutricion/Bebidas_espirituosas-Alcohol-Energia-Nutricion_388473428_119607645_1706x1280.jpg',
        descripcion:"Todo tipo de whisky",
        id:2
    },{
        title: 'Vodka',
        url: 'https://res.cloudinary.com/postedin/image/upload/d_cookcina:no-image.jpg,w_340,c_thumb,f_auto,q_80/cookcina/i-10388.jpeg',
        descripcion:"Todo tipo de vodka",
        id:2
    },{
        title: 'Tequila',
        url: 'https://static.iris.net.co/dinero/upload/images/2013/7/31/180946_162720_1.jpg',
        descripcion:"Todo tipo de tequila",
        id:2
    }
] 