import React, { useState,Component } from 'react'
import { Button,TextInput,Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import firebase from '../util/firebase'
import TextField from '@material-ui/core/TextField';



class confirCompra extends Component {

    constructor(props) {
        super(props);

        this.state = {
            lista: this.props.listaCarro
        };


        this.state1 = {
            nombre: "",
            apellidos: "",
            direccion: "",
            telefono:"",
          };

    }

    inputValueUpdate=(val,prop)=>{
        const state = this.state1;
        state[prop] = val;
        this.setState(state);
    };

    
   
    newOrden() {
         
         if(this.state.nombres === ' '){
             alert('Ingrese un nombre')
         } if(this.state.apellidos === ' '){
            alert('Ingrese un apellido')
        } if(this.state.direccion === ' '){
            alert('Ingrese una direccion')
        } if(this.state.telefono === ' '){
            alert('Ingrese un telefono')
        }else{
            firebase.db.collection('Datos').add({
               nombres: this.state1.nombres,
               apellidos: this.state1.apellidos,
               direccion: this.state1.direccion,
               telefono: this.state1.telefono
           }).then((res)=>{
               this.setState({
                nombres: '',
                apellidos: '',
                direccion: '',
                telefono: ''
               })

           });
           alert('guardado');
           this.props.navigation.navigate('Home')
           
        }
     }

    componentWillUpdate(nextProps, nextState) {
            nextState.lista =nextProps.listaCarro     
    }

    removeItem=(e)=>{
        this.props.remove(e)
    }



    render() {


       

        const simpleAlertHandler = () => {
            //function to make simple alert
            alert('Datos almacenados');
          };

        var total = 0;

        this.state.lista.map(e => {

            total += e.Total
        })

        return (
            <View>
                <View style={styles.containerLista}>
                <text style={{ textAlign: 'center', fontWeight: 600, fontSize: 20 }}>Ingrese los datos </text>

                <TextInput
                style={{ width:280,heigh:30, fontSize: 18}}
                placeholder="Nombres"  
                onChangeText={(value) => this.inputValueUpdate(value, "nombres")}
                />
                <TextInput
                style={{ width:280,heigh:30, fontSize: 18}}
                placeholder="Apellidos"       
                onChangeText={(value) => this.inputValueUpdate(value, "apellidos")}
                />
                <TextInput
                style={{ width:280,heigh:30, fontSize: 18}}
                placeholder="Direccion"  
                onChangeText={(value) => this.inputValueUpdate(value, "direccion")}  
                />
                <TextInput 
                 style={{ width:280,heigh:30, fontSize: 18}}
                 placeholder="Telefono"  
                onChangeText={(value) => this.inputValueUpdate(value, "telefono")}     
                />
                 
                    {this.state.lista.map((e, index) => {
                        return (
                            <View key={index} style={{ flex: 1, flexDirection: 'row', alignItems: "center", borderBottomWidth: 0.3, borderBottomColor: "#eee" }}>
                                <TouchableOpacity style={{marginHorizontal:10}} onPress={()=>this.removeItem(e)}><Text style={{color:"red",fontWeight:600}}>X</Text></TouchableOpacity>
                                <Text style={{ fontSize: 18, fontWeight: 600, marginVertical: 5, flex: 3 }}>{e.Nombre}(x{e.Cantidad})</Text>
                                <Text style={{ textAlign: 'right', fontWeight: 600, fontSize: 18, flex: 3}}>{e.Total} $</Text>
                            </View>
                        )
                    })}

                    {total > 0 ?
                        <View style={styles.Total}>
                            <Text style={{ textAlign: 'right', fontWeight: 600, fontSize: 20 }}>{total} $</Text>
                        </View>
                        : <Text style={{ textAlign: 'right', fontWeight: 600, fontSize: 20 }}>No hay productos </Text>
                    }

                    <TouchableOpacity style={styles.button} onPress={() => this.newOrden()}>
                        <Text>Envio</Text>
                    </TouchableOpacity>
                       
                </View>

            </View>
        )
    }
}




export default confirCompra;

const styles = StyleSheet.create({

    button: {
        backgroundColor: "#ff0099",
        padding: 10,
        width: 150
    },
    containerLista: {
        backgroundColor: "#fff",
        padding: 10

    }

})
