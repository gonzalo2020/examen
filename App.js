import React,{Component} from 'react';
import { View, Text,StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import Home from './screem/Home'
import Compra from './screem/confirCompra'
import Envio from './screem/envio'

const Stack = createStackNavigator();



class App extends Component {

    constructor(){
      super();

      this.state ={
        listaCarrito:[]
      }
    }

  lista(l){
    
    this.setState({
      listaCarrito:l
    })
  }

  removeItem(item){

    

      var nuevaLista  = this.state.listaCarrito.filter(e=>{
        return e.Id !== item.Id;
    });



    this.setState({
      listaCarrito : nuevaLista
    })


  }

  render() {
    return (
      <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home"  options={({navigation,route})=>({
           headerTitle:'El Carro Bar',
           headerRight: () => (
             <Text onPress={()=>navigation.navigate('Compra')} style={styles.btnCar}>Carrito</Text>
           )
        })} >
           {props => <Home {...props} carrito={(l)=>this.lista(l)}  listaCarro={this.state.listaCarrito} />}
        </Stack.Screen>
        <Stack.Screen name="Compra" >
               {props => <Compra {...props} listaCarro={this.state.listaCarrito} remove={(item)=>this.removeItem(item)}/>}
        </Stack.Screen>
        <Stack.Screen name="Envio" component={Envio} />
      </Stack.Navigator>
    </NavigationContainer>
    )
  }
}



export default App;


const styles = StyleSheet.create({

  btnCar: {
      paddingRight: 10
  },
 
})
