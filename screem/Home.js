import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity,Modal} from "react-native"
import Carousel from '../component/Carousel'
import { CommonActions } from '@react-navigation/native';
import { dummyData } from '../data/Data'

const width = window.screen.width
const heigth = window.screen.height

class Home extends Component {
    constructor(props){
            super(props);

            this.state={
                list:this.props.listaCarro,
                carro:0,
                modal:false,
                producto:{
                    Id:0,
                    Nombre:'',
                    Precio:0,
                    Cantidad:0,
                    Total:0
                }
            }
    }

    componentWillUpdate(nextProps, nextState) {
  

        nextState.list =nextProps.listaCarro

   
    }

    anadirCarro=(Id,Nombre,Precio)=>{

       let producto = this.state.list.find(e=>{

            return e.Id === Id
        })

  
        if(producto!=undefined){

            this.setState({
                modal:true,
                producto
            })

        }else{

            this.setState({
                modal:true,
                producto:{
                    Id,
                    Nombre,
                    Precio,
                    Cantidad:1,
                    Total:Precio
                }
            })

        }
        

     
    }

    increment=()=>{

        var producto = this.state.producto
        producto.Cantidad = producto.Cantidad +1
        
        producto.Total = producto.Precio * producto.Cantidad

        this.setState({
                producto
       
        })
    }

    decrement=()=>{
        var producto = this.state.producto
       

        if(producto.Cantidad > 1){

            producto.Cantidad = producto.Cantidad -1

            producto.Total = producto.Precio * producto.Cantidad

            this.setState({
                    producto
        
            })
        }
    }

    Cancelar=()=>{
        this.setState({
            modal:false
        })
    }

    GuardarCarrito=()=>{

        let indice =  this.state.list.findIndex(e => e.Id === this.state.producto.Id);

        if(indice == -1){
            this.state.list.push(this.state.producto)
        }
        else{
            this.state.list[indice] = this.state.producto
        }
 
       
    

        this.setState({
            modal:false
        })

        this.props.carrito(this.state.list)

     
    }

    render() {

    
        
        return (
            <View >
                <Carousel data={dummyData} />

                <View style={styles.container} >
                    <TouchableOpacity style={styles.card} onPress={()=>{this.anadirCarro(1,'Cerveza de Barril',28)}}>
                        <Image style={styles.imgCard} source={{ uri: 'https://i.blogs.es/8cc948/barril-heineken1/450_1000.jpg' }} />
                        <Text>Cerverza de barril</Text>
                        <Text>28$</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.card} onPress={()=>{this.anadirCarro(2,'Enlatada',12)}}>
                        <Image style={styles.imgCard} source={{ uri: 'https://www.frecuento.com//medias/40309427-01-BASEIMAGE-Midres?context=bWFzdGVyfGZyZWN1ZW50b3w0NzYwNTZ8aW1hZ2UvanBlZ3xmcmVjdWVudG8vaGNmL2g1MC85MDQxNzczOTIwMjg2LmpwZ3xhOGU1MWFlMDNkYzY3MGE1OTI1MWEzYjIyNzg0NjdiYzI1Y2ViYWNiMzgxMWQ5MmEwZmFjN2UyZTQ1Y2ZmNjFk' }} />
                        <Text>Six pack</Text>
                        <Text>12$</Text>
                    </TouchableOpacity>
                    
                    {/* <TouchableOpacity style={styles.card} onPress={()=>{this.anadirCarro(2,'Enlatada',12)}}>
                        <Image style={styles.imgCard} source={{ uri: 'https://andesbrew.com/wp-content/uploads/2020/05/Red-600x600.jpg' }} />
                        <Text>cerveza artesanal</Text>
                        <Text>5$</Text>
                    </TouchableOpacity> */}


                </View>

                <Text>{this.state.carro}</Text>
                
                {this.state.modal?
       
                    <View  style={styles.modal}>
                        <View style={styles.containerModal}>
                        <Text>Ingrese la cantidad</Text>
                            <Text style={{fontSize:20,fontWeight:600,marginVertical:5}}>{this.state.producto.Nombre}</Text>                     
                            <View style={styles.boxCount}>
                                <Text style={styles.contador}>{this.state.producto.Cantidad}</Text>
                                <TouchableOpacity onPress={()=>this.increment()} style={styles.btnSum}><Text>+</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.decrement()} style={styles.btnSum}><Text>-</Text></TouchableOpacity>
                            </View>
                            <Text style={{textAlign:'right',fontWeight:600,fontSize:20}}>{this.state.producto.Total} $</Text>

                            <View style={styles.containerBtnCarro}>
                              <TouchableOpacity onPress={()=>this.Cancelar()} style={styles.btnCancelar}><Text style={{color:"#fff"}}>Cancelar</Text></TouchableOpacity>
                              <TouchableOpacity onPress={()=>this.GuardarCarrito()} style={styles.btnAñadir}><Text style={{color:"#fff"}}>Añadir</Text></TouchableOpacity>
                            </View>
                        </View>
                    </View>
                :null
                }

            </View>

        )
    }
}

export default Home

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnMenu: {
        margin: 10,
        color: "blue"
    },
    btnCar: {
        paddingRight: 10
    },
    card: {
        width:300,
        marginVertical:20,
        backgroundColor:"#fff",
        borderRadius:10
    },
    imgCard: {
        width: '100%',
        height: 200
    },
    modal:{
        flex:1,
        backgroundColor:'#fff',
        position:'absolute',
        borderWidth:0,
        justifyContent:'center',
        alignItems:'center',
        width:width,
        height:heigth,
        backgroundColor:'rgba(0,0,0,0.6)'
    },
    containerModal:{
        padding:10,
        backgroundColor:"#fff",
        width:200,
        borderRadius:10
  
    },
    boxCount:{
        flex:1,
        flexDirection:'row',
        marginVertical:20
  
        
    },
    btnSum:{
        marginLeft:10,
        color:"#333",
        fontSize:18,
        height:30,
        width:50,
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:"#ff0099"
    },
    contador:{
        backgroundColor:"#eee",
        padding:5,
        height:30,
        width:100
    },
    containerBtnCarro:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        marginVertical:20
  
    },
    btnCancelar:{
        backgroundColor:"#ff9900",
        width:60,
        marginRight:10,
        height:30,
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    btnAñadir:{
        backgroundColor:"green",
        width:60,
        height:30,
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    }

})
