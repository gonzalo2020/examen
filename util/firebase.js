import  firebase from 'firebase';
import 'firebase/firestore';


var firebaseConfig = {
    apiKey: "AIzaSyCaAsE8RPZvDt1PlWX6ARSZ1zgSu8513zQ",
    authDomain: "carrito-b04fe.firebaseapp.com",
    projectId: "carrito-b04fe",
    storageBucket: "carrito-b04fe.appspot.com",
    messagingSenderId: "986013378336",
    appId: "1:986013378336:web:6c82aa4519b85d0f8b5945",
    measurementId: "G-DJ9BKJR9LR"
  };

  firebase.initializeApp(firebaseConfig);
  const db = firebase.firestore();

  export default{
    firebase,
    db
  }
  